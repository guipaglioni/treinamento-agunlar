export class Pessoa {
    constructor(
        public id?: number,
        public nome?: string,
        public dependentes = 0,
        public nascimento?: Date
        ){}
}

