import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PessoasRoutingModule } from './pessoas-routing.module';
import { ListarComponent } from './components/listar';
import { CadastrarComponent } from './components/cadastrar';
import { EditarComponent } from './components/editar';
import { PessoaService } from './services';
import { NumeroDirective } from './diretivas/numero.directive';
import { DataPipe } from './pipes/data.pipe';


@NgModule({
  declarations: [
    ListarComponent,
    CadastrarComponent,
    EditarComponent,
    NumeroDirective,
    DataPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    PessoasRoutingModule
  ],
  exports: [
    ListarComponent,
    CadastrarComponent,
    EditarComponent
  ],
  providers: [
    PessoaService
  ]
})
export class PessoasModule { }
