import { Component, OnInit } from '@angular/core';
import { PessoaService } from '../../services';
import { Pessoa } from '../../models';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  pessoas: Pessoa[];

  constructor(
    private listarService: PessoaService
  ) { }

  ngOnInit() {
    this.pessoas = this.listarService.listarPessoas();
  }
  /**
   * Lista todas as pessoas
   * @return Pessoa[] 
   */
  listarPessoas(): Pessoa[] {
    return this.listarService.listarPessoas()
  }

  /**
   * Remove a pessoa selecionada
   * @param $event any
   * @param Pessoa pessoa 
   */
  remover($event: any, pessoa: Pessoa): void {
    $event.preventDefault();
    if (confirm('Deseja remover: ' + pessoa.nome + '?')) {
      this.listarService.remover(pessoa.id);
      this.pessoas = this.listarPessoas();
    }

  }

}
