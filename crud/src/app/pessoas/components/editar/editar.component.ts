import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Pessoa } from '../../models';
import { PessoaService } from '../../services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
  @ViewChild('formPessoa', {static: true}) formPessoa: NgForm;
  pessoa: Pessoa;
  constructor(
    private pessoaService: PessoaService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    let id =+this.route.snapshot.params['id'];
    this.pessoa = this.pessoaService.bucaPorId(id);
  }

  atualizar(): void {
    if ( this.formPessoa.form.valid) {
      this.pessoaService.atualizar(this.pessoa);
      this.router.navigate(['/pessoas'])
    }
  }

}
