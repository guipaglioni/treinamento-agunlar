import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Pessoa } from '../../models';
import { PessoaService } from '../../services';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css']
})
export class CadastrarComponent implements OnInit {

  @ViewChild('formPessoa' ,{static: true}) formPessoa: NgForm;
  pessoa: Pessoa;

  constructor(
    private pessoaService: PessoaService,
    private router: Router
  ) { }

  ngOnInit() {
    this.pessoa = new Pessoa();
  }

  cadastrar(): void { 
    if ( this.formPessoa.form.valid ) {
      this.pessoaService.cadastrar(this.pessoa);
      this.router.navigate(['/pessoas'])
    }
   }

}
