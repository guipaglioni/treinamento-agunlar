import { Injectable } from '@angular/core';

import { Pessoa } from '../models';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  pessoa: Pessoa[];

  constructor() { }
  /** Lista todas as pessoas contidas no local storage
   *  @return void 
   */
  listarPessoas (): Pessoa[] {
    const pessoas = localStorage['pessoas']
    return pessoas? JSON.parse(pessoas): [];
  }

  /** Cadastra uma nova pessoa 
   *  @return void
   *  @param pessoa Pessoa
   */
  cadastrar(pessoa: Pessoa): void {
    const pessoas = this.listarPessoas();
    pessoa.id = new Date().getTime();
    pessoas.push(pessoa);
    localStorage['pessoas'] = JSON.stringify(pessoas);
  }

  /** Busca por id
   *  @param id number
   */
  bucaPorId(id: number): any {
    const pessoas: Pessoa[] = this.listarPessoas();
    return pessoas.find(pessoa => pessoa.id === id)
  }

  /** Atualiza uma Pessoa já existente 
   *  @param Pessoa pessoa                         
   */
  atualizar(pessoa: Pessoa): void {
    const pessoas: Pessoa[] = this.listarPessoas();
    pessoas.forEach((obj, index, objs) =>{
      if (pessoa.id === obj.id) {
        objs[index] = pessoa
      }
    });
    localStorage['pessoas'] = JSON.stringify(pessoas);
  }
  /**
   * Remove uma pessoa pelo id 
   * @param id number
   */
  remover(id: number): void {
    let pessoas: Pessoa[] = this.listarPessoas();
    pessoas = pessoas.filter( pessoa => pessoa.id !== id);
    localStorage['pessoas'] = JSON.stringify(pessoas);
  }
}
