import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PessoasRoutingComponent } from './pessoas-routing.component';
import { ListarComponent, CadastrarComponent, EditarComponent } from './components';


const routes: Routes = [
  {
    path: 'pessoas',
    component: PessoasRoutingComponent,
    children: [
      {
        path: '',
        component: ListarComponent
      },
      {
        path: 'cadastrar',
        component: CadastrarComponent
      },
      { 
        path: 'editar/:id',
        component: EditarComponent
      }
    ]
  }
];

@NgModule({
  declarations: [PessoasRoutingComponent],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PessoasRoutingModule { }
